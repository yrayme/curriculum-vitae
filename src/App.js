import { ThemeProvider } from '@material-ui/styles';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Cv from './Components/Cv';
/*Constantes */
import { CV_THEME } from './Components/Cv/constants';

const theme = CV_THEME;

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <ThemeProvider theme={theme} >
          <Route exact path="/" component={Cv} />
        </ThemeProvider>
      </Switch>
    </BrowserRouter>

  );
}

export default App;
