import { createMuiTheme, makeStyles } from '@material-ui/core/styles';
import fondo from './img/fondo.jpg';


export const CV_THEME = createMuiTheme({
  typography: {
    fontFamily: 'Gotham',
    fontStyle: 'normal',
    fontWeight: 'normal',
    src: 'url(../../public/font/gothamotf)',
  },
  palette: {
    primary: {
      main: '#1A5276',
    },
    secondary: {
      main: '#4D4D4D',
    }
  },
  overrides: {
    MuiTab: {
      textColorSecondary: {
        color: 'purple',
      }
    },
  }
});

export const styleCV = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },

  bottom: {
    color: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
  },

  top: {
    color: '#F1C40F',
    animationDuration: '550ms',
    position: 'absolute',
    left: 0,
  },

  header: {
    backgroundImage: 'url(' + fondo + ')',
    backgroundSize: '100% 100%',
    backgroundRepeat: 'no-repeat'

  },

  img: {
    borderRadius: '5vh',
    width: 100,
    height: 230,
    [theme.breakpoints.down('xs')]: {
      animation: "floatTop",
      animationDuration: "6s",
      animationIterationCount: "infinite",
    },
    [theme.breakpoints.up('md')]: {
      animation: "floatRight",
      animationDuration: "6s",
      animationIterationCount: "infinite",
    },
  },

  name: {
    color: 'black',
    textAlign: 'center',
    [theme.breakpoints.down('sm')]: {
      fontSize: '35px',
      '-webkit-text-stroke': '2px #1C2833',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '60px',


    }
  },
  title: {
    [theme.breakpoints.down('sm')]: {
      fontSize: '20px'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '25px',

    }
  },
  profesion: {
    color: 'black',
    textAlign: 'center',
    [theme.breakpoints.down('sm')]: {
      fontSize: '20px',
      '-webkit-text-stroke': '1px #1C2833',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '25px',

    }
  },
  subtitle: {
    [theme.breakpoints.down('sm')]: {
      fontSize: '25px'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '40px',

    }
  },
  fund: {
    background: 'white',
    position: "relative",
    // background: '-webkit-linear-gradient(to right, #59c173, #a17fe0, #5d26c1)',
    // background: 'linear-gradient(to right, #59c173, #a17fe0, #5d26c1)',
    // background: '#654ea3',
    // background: '-webkit-linear-gradient(to right, #eaafc8, #654ea3)',
    // background: 'linear-gradient(to right, #eaafc8, #654ea3)',

  },
  text: {
    [theme.breakpoints.down('sm')]: {
      fontSize: '15px'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '18px',
      textAlign: 'justify'
    }
  },


  Contname: {
  },
  pdf: {
    // marginTop: "30vh",
    width: 50,
    backgroundColor: "#FFC0CB",
    position: 'absolute',
    top: 10,
    right: 40,
    zIndex: 100,
    boxShadow: '1px 2px 2px',
    "&:hover ": {
      backgroundColor: 'purple',
    },
    [theme.breakpoints.down('sm')]: {
      marginLeft: 0,
    },
  },
  titles: {
    display: "flex",
    gap: "20px",
    alignItems: "center",
    width: "100%"
  },
  col1:{
    background: "#C8C8C8",
    padding: "0px 40px"
  },
  col2:{
    padding: "0px 40px"
  },
  skills: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 5
  }
}))