import React, { useState, useEffect } from 'react';
import { Grid, Paper, Typography, Divider, AppBar, Tabs, Tab, Box, CircularProgress, IconButton } from '@material-ui/core';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import PersonIcon from '@material-ui/icons/Person';
import CakeIcon from '@material-ui/icons/Cake';
import HomeIcon from '@material-ui/icons/Home';
import WorkIcon from '@material-ui/icons/Work';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import SchoolIcon from '@material-ui/icons/School';
import BookIcon from '@material-ui/icons/Book';
import ImportantDevicesIcon from '@material-ui/icons/ImportantDevices';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import MailIcon from '@material-ui/icons/Mail';
import GitHubIcon from '@material-ui/icons/GitHub';
import LinkedInIcon from '@material-ui/icons/LinkedIn';



import foto from './img/cv.jpeg';

import { styleCV } from './constants';

import './css/index.css'

export default function Cv() {
  const [value, setValue] = useState(0);
  const [react, setReact] = useState(10);
  const [html, setHtml] = useState(10);
  const [javaScript, setJavaScript] = useState(10);
  const [php, setPhp] = useState(10);
  const [python, setPython] = useState(10);
  const [odoo, setOdoo] = useState(10);
  const [git, setGit] = useState(10);
  const [scrum, setScrum] = useState(10);
  const [native, setNative] = useState(10);
  const [typescript, setTypescript] = useState(10);

  useEffect(() => {
    const timer = setInterval(() => {
      setReact((prevReact) => (prevReact >= 90 ? 90 : prevReact + 10));
      setHtml((prevHtml) => (prevHtml >= 90 ? 90 : prevHtml + 10));
      setJavaScript((prevJavaScript) => (prevJavaScript >= 80 ? 80 : prevJavaScript + 10));
      setPhp((prevPhp) => (prevPhp >= 40 ? 40 : prevPhp + 10));
      setPython((prevPython) => (prevPython >= 40 ? 40 : prevPython + 10));
      setOdoo((prevOdoo) => (prevOdoo >= 80 ? 80 : prevOdoo + 10));
      setGit((prevGit) => (prevGit >= 70 ? 70 : prevGit + 10));
      setScrum((prevScrum) => (prevScrum >= 90 ? 90 : prevScrum + 10));
      setNative((prevScrum) => (prevScrum >= 60 ? 60 : prevScrum + 10));
      setTypescript((prevScrum) => (prevScrum >= 60 ? 60 : prevScrum + 10));
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  }, []);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };


  const classes = styleCV();
  return (
    <Grid container >
      <Grid xs={12} md={2} />
      <Grid xs={12} md={8} >
        <Paper elevation={3} className={classes.fund} >
          <div>
            <IconButton className={classes.pdf} >
              <a href={process.env.PUBLIC_URL + '/CVYR.pdf'} download='Curriculum-Yamile.pdf'>
                <PictureAsPdfIcon style={{ color: "white" }} />
              </a>
            </IconButton >
          </div>
          {/* HEADER */}
          <Grid xs={12} md={12} container justify='center' style={{display: "flex", alignItems: "center"}}>
            <Grid item xs={12} sm={4} md={4} className={classes.col1}>
              <img src={foto} alt='img' className={classes.img} style={{ objectFix: 'cover', width: '100%', margin: "30px 0px" }} />
            </Grid>
            <Grid item xs={12} sm={8}>
              <Grid container>
                <Grid xs={12} sm={12} md={12} className={classes.Contname}>
                  <Typography className={classes.name}>Yamile Rayme </Typography>
                  <Typography className={classes.profesion} >Ingeniera de Sistemas </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          {/* BODY */}
          <Grid container xs={12} md={12} sm={12}>
          {/* PRIMERA COLUMNA */}
            <Grid item xs={12} sm={4} md={4} className={classes.col1}>
              <Grid container xs={12} sm={12} md={12}>
                {/* PERFIL */}
                <Grid item xs={12} className={classes.titles} style={{marginTop: "20px"}}>
                  <div><LocalLibraryIcon style={{ color: '#633974', marginTop: 10 }} /></div>
                  <Typography color='primary' className={classes.title}>Perfíl</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Divider style={{ backgroundColor: '#633974', marginBottom: 10 }} />
                </Grid>
                <Grid item xs={12} sm={12} md={12} >
                  <Typography color='secondary' className={classes.text}>
                    Ingeniera de Sistemas enfocado en el área de
                  desarrollo Web, me defino como una persona proactiva, responsable, organizada,
                  puntual, creativa y analítica. Con capacidad de rápido aprendizaje
                  y de trabajar en equipo en situaciones bajo presión y sin supervisión constante. 
                  </Typography>
                </Grid>

              {/* DATOS PERSONALES */}
                <Grid item xs={12} className={classes.titles} style={{marginTop: "20px"}}>
                  <div><PersonIcon style={{ color: '#633974', marginTop: 10 }} /></div>
                  <Typography color='primary' className={classes.title}>Datos personales</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Divider style={{ backgroundColor: '#633974', marginBottom: 10 }} />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <div style={{marginLeft: "30px", marginTop: "20px"}}>
                    <div className={classes.titles}>
                      <div><CakeIcon fontSize='small' color='primary' /></div>
                      <Typography color='secondary' className={classes.text}>Datos personales</Typography>
                    </div>
                    <div className={classes.titles}>
                      <div><HomeIcon fontSize='small' color='primary' /></div>
                      <Typography color='secondary' className={classes.text}>Urbanización el Silencio, Venezuela - Caracas </Typography>
                    </div>
                    <div className={classes.titles}>
                      <div><WhatsAppIcon fontSize='small' color='primary' /></div>
                      <Typography color='secondary' className={classes.text}>+58412-9569788</Typography>
                    </div>
                    <div className={classes.titles}>
                      <div><MailIcon fontSize='small' color='primary' /></div>
                      <Typography color='secondary' className={classes.text}>rayme295@gmail.com </Typography>
                    </div>
                    <div className={classes.titles}>
                      <div><GitHubIcon fontSize='small' color='primary' /></div>
                      <a href='https://github.com/yrayme/' style={{textDecoration: "none"}}>
                        <Typography color='secondary' className={classes.text}>yrayme</Typography>
                      </a>
                    </div>
                    <div className={classes.titles}>
                      <div><LinkedInIcon fontSize='small' color='primary' /></div>
                      <a href='https://www.linkedin.com/in/yamile-rayme-597b46185/' style={{textDecoration: "none"}}>
                        <Typography color='secondary' className={classes.text}>Yamile Rayme</Typography>
                      </a>
                    </div>
                  </div>
                </Grid>
              {/* APTITUDES */}
                <Grid item xs={12} className={classes.titles} style={{marginTop: "20px"}}>
                  <div><EmojiPeopleIcon style={{ color: '#633974', marginTop: 10 }} /></div>
                  <Typography color='primary' className={classes.title}>Hobbies</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Divider style={{ backgroundColor: '#633974', marginBottom: 10 }} />
                </Grid>
                <Grid item xs={12} sm={12} md={12} style={{marginTop: "20px"}}>
                  <ul style={{ marginTop: 0 }}>
                    <li><Typography color='secondary' className={classes.text}>Bailar</Typography></li>
                    <li><Typography color='secondary' className={classes.text}>Ver Peliculas</Typography></li>
                    <li><Typography color='secondary' className={classes.text}>Salir los viernes y fines de semana </Typography></li>
                    <li><Typography color='secondary' className={classes.text}>Escuchar música</Typography></li>
                    <li><Typography color='secondary' className={classes.text}>Jugar con mis perros</Typography></li>
                  </ul>
                </Grid>
              </Grid>
            </Grid>
            {/* SEGUNDA COLUMNA */}
            <Grid item xs={12} sm={8} md={8}>
              {/* EXPERIENCIA */}
              <Grid container xs={12} sm={12} md={12} className={classes.col2}>
                <Grid item xs={12} className={classes.titles} style={{marginTop: "20px"}}>
                  <div><WorkIcon style={{ color: '#633974', marginTop: 10 }} /></div>
                  <Typography color='primary' className={classes.title}>Experiencia</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Divider style={{ backgroundColor: '#633974', marginBottom: 10 }} />
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <Paper style={{ border: '1px solid #1A5276', marginTop: 20 }}  >
                    <div className={classes.root} style={{ background: 'white' }} >
                      <AppBar position="static" style={{ background: '#FFC0CB', color: "black" }}>
                        <Tabs
                          value={value}
                          onChange={handleChange}
                          aria-label="nav tabs example"
                        >
                          <Tab label="OnlineSat" {...a11yProps(0)} />
                          <Tab label="Shokwork" {...a11yProps(1)} />
                          <Tab label="Vilo Technologies" {...a11yProps(2)} />
                          <Tab label="KMG" {...a11yProps(3)} />
                          <Tab label="Tysamnca"  {...a11yProps(4)} />
                          <Tab label="Softnet & Systems" {...a11yProps(5)} />
                        </Tabs>
                      </AppBar>
                      <TabPanel value={value} index={0}>
                        <Grid container xs={12} sm={12} md={12}>
                          <Grid xs={12} sm={7} md={7} >
                            <Typography style={{ color: '#633974' }} className={classes.text}>Desarrollador Frontend Senior</Typography>
                          </Grid>
                          <Grid xs={12} sm={5} md={5} container justify='flex-end'>
                            <Typography style={{ color: '#633974' }} className={classes.text}>Octubre 2022 - Actualmente</Typography>
                          </Grid>
                          <Grid container xs={12} sm={12} md={12} style={{marginTop: 10}}>
                            <Grid xs={12} sm={12} md={12} >
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Entregar código de producción robusto y de alta calidad para una amplia gama de proyectos dentro de la empresa.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Trabajar con tecnologías como React, NextJs, typescript y javascript.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Utilizar librerías como Redux, Formik, yup, next-auth, react-hook-form, entre otras.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Diseñar una aplicación de seguros para la empresa con figma y luego maquetar la misma app.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Reuniones con clientes para determinar el camino mas amigable y confiable para las aplicaciones.</li>
                            </Grid>
                          </Grid>
                        </Grid>
                      </TabPanel>
                      <TabPanel value={value} index={1}>
                        <Grid container xs={12} sm={12} md={12}>
                          <Grid xs={12} sm={7} md={7} >
                            <Typography style={{ color: '#633974' }} className={classes.text}>Desarrollador Frontend</Typography>
                          </Grid>
                          <Grid xs={12} sm={5} md={5} container justify='flex-end'>
                            <Typography style={{ color: '#633974' }} className={classes.text}>Octubre 2021 - 2022</Typography>
                          </Grid>
                          <Grid container xs={12} sm={12} md={12} style={{marginTop: 10}}>
                            <Grid xs={12} sm={12} md={12} >
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Trabajar con tecnologías como React, NextJs, React Native, typescript y javascript.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Utilizar librerías como Redux, react-hook-form, entre otras.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Utilizar tailwincss, Material ui para construir y diseñar páginas web de forma rápida y eficiente.</li>
                            </Grid>
                          </Grid>
                        </Grid>
                      </TabPanel>
                      <TabPanel value={value} index={2}>
                        <Grid container xs={12} sm={12} md={12}>
                          <Grid xs={12} sm={7} md={7} >
                            <Typography style={{ color: '#633974' }} className={classes.text}>Desarrollador Frontend</Typography>
                          </Grid>
                          <Grid xs={12} sm={5} md={5} container justify='flex-end'>
                            <Typography style={{ color: '#633974' }} className={classes.text}>June 2021 - 2023</Typography>
                          </Grid>
                          <Grid container xs={12} sm={12} md={12} style={{marginTop: 10}}>
                            <Grid xs={12} sm={12} md={12} >
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Trabajar con tecnologías como React, NextJs y javascript.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Utilizar librerías como Redux, Formik, entre otras.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Utilizar Material ui para construir y diseñar páginas web de forma rápida y eficiente.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Reuniones con clientes para determinar el camino mas amigable y confiable para las aplicaciones.</li>
                            </Grid>
                          </Grid>
                        </Grid>
                      </TabPanel>
                      <TabPanel value={value} index={3}>
                        <Grid container xs={12} sm={12} md={12}>
                          <Grid xs={12} sm={7} md={7} >
                            <Typography style={{ color: '#633974' }} className={classes.text}>Desarrollador Frontend Junior</Typography>
                          </Grid>
                          <Grid xs={12} sm={5} md={5} container justify='flex-end'>
                            <Typography style={{ color: '#633974' }} className={classes.text}>Octubre 2019 - 2021</Typography>
                          </Grid>
                          <Grid container xs={12} sm={12} md={12} style={{marginTop: 10}}>
                            <Grid xs={12} sm={12} md={12} >
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Desarrollo de aplicaciones Web para clientes de la empresa utilizando
                                                        JavaScript, React y .net core.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Maquetación HTML5, CSS3, Bootstrap, Reactjs.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Integración de Apis en las aplicaciones.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Mantenimiento de aplicaciones Web.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Creación de guías para cada cliente.</li>
                            </Grid>
                          </Grid>
                        </Grid>

                      </TabPanel>
                      <TabPanel value={value} index={4}>
                        <Grid container xs={12} sm={12} md={12}>
                          <Grid xs={12} sm={7} md={7} >
                            <Typography style={{ color: '#633974' }} className={classes.text}>Desarrollador Junior</Typography>
                          </Grid>
                          <Grid xs={12} sm={5} md={5} container justify='flex-end'>
                            <Typography style={{ color: '#633974' }} className={classes.text}>Octubre 2018 – 2019</Typography>
                          </Grid>

                          <Grid container xs={12} sm={12} md={12} style={{marginTop: 10}}>
                            <Grid xs={12} sm={12} md={12} >
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Desarrollo de actualizaciones y nuevos módulos para el ERP Odoo en
                                                            sus versiones 10, 11, 12.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>trabajando con el controlador de versiones git.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Creación de guías para cada módulo creado.</li>
                            </Grid>
                          </Grid>
                        </Grid>
                      </TabPanel>
                      <TabPanel value={value} index={5}>
                        <Grid container xs={12} sm={12} md={12}>
                          <Grid xs={12} sm={7} md={7} >
                            <Typography style={{ color: '#633974' }} className={classes.text}>Desarrollador Junior</Typography>
                          </Grid>
                          <Grid xs={12} sm={5} md={5} container justify='flex-end'>
                            <Typography style={{ color: '#633974' }} className={classes.text}>05/2018 – 10/2018</Typography>
                          </Grid>

                          <Grid container xs={12} sm={12} md={12} style={{marginTop: 10}}>
                            <Grid xs={12} sm={12} md={12} >
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Realización de pasantias bajo el módulo Punto de ventas de odoo.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Desarrollo de actualizaciones y nuevos módulos para el ERP Odoo en
                                                            sus versiones 10.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>trabajando con el controlador de versiones git.</li>
                              <li style={{ color: '#4D4D4D' }} className={classes.text}>Creación de guías para cada módulo creado.</li>
                            </Grid>
                          </Grid>
                        </Grid>
                      </TabPanel>
                    </div>
                  </Paper>
                </Grid>
              {/* EDUCACIÓN */}
                <Grid item xs={12} className={classes.titles} style={{marginTop: "20px"}}>
                  <div><SchoolIcon style={{ color: '#633974', marginTop: 10 }} /></div>
                  <Typography color='primary' className={classes.title}>Formación</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Divider style={{ backgroundColor: '#633974', marginBottom: 10 }} />
                </Grid>
                <Grid item xs={12} style={{marginLeft: "30px", marginTop: "15px"}}>
                  <Grid container>
                    <Grid xs={12} sm={6} md={6} >
                      <Typography style={{ color: '#633974' }} className={classes.text}>Ingeniera de Sistemas</Typography>
                    </Grid>
                    <Grid xs={12} sm={4} md={4} container justify='flex-end'>
                      <Typography style={{ color: '#633974' }} className={classes.text}>2012 – 2018</Typography>
                    </Grid>
                    <Grid xs={12} sm={2} md={2} />
                    <Grid xs={12} sm={7} md={7} container justify='flex-end'>
                      <Typography color='secondary' className={classes.text}><i>Universidad Nacional Experimental Politécnica "Antonio José De Sucre”</i></Typography>
                    </Grid>
                  </Grid>                    
                </Grid>

              {/* HABILIDADES */}
                <Grid item xs={12} className={classes.titles} style={{marginTop: "20px"}}>
                  <div><ImportantDevicesIcon style={{ color: '#633974', marginTop: 10 }} /></div>
                  <Typography color='primary' className={classes.title}>Habilidades</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Divider style={{ backgroundColor: '#633974', marginBottom: 10 }} />
                </Grid>
                <Grid item xs={12} style={{marginLeft: "30px"}}>
                  <Grid container spacing={4}>
                    <Grid item xs={12} sm={6}>
                      <div className={classes.skills}>
                        <Typography color='secondary' className={classes.text} >Reactjs/Redux/NextJs</Typography>
                        <CircularProgressWithLabel value={react} />
                      </div>
                      <div className={classes.skills}>
                        <Typography color='secondary' className={classes.text} >HTML5/CSS3</Typography>
                        <CircularProgressWithLabel value={html} />
                      </div>
                      <div className={classes.skills}>
                        <Typography color='secondary' className={classes.text} >Metodología Scrum</Typography>
                        <CircularProgressWithLabel value={scrum} />
                      </div>
                      <div className={classes.skills}>
                        <Typography color='secondary' className={classes.text} >Javascript</Typography>
                        <CircularProgressWithLabel value={javaScript} />
                      </div>
                      <div className={classes.skills}>
                        <Typography color='secondary' className={classes.text} >Odoo V.10, V.11</Typography>
                        <CircularProgressWithLabel value={odoo} />
                      </div>
                      <div className={classes.skills}>
                        <Typography color='secondary' className={classes.text} >Git</Typography>
                        <CircularProgressWithLabel value={git} />
                      </div>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <div className={classes.skills}>
                        <Typography color='secondary' className={classes.text} >Figma</Typography>
                        <CircularProgressWithLabel value={git} />
                      </div>
                      <div className={classes.skills}>
                        <Typography color='secondary' className={classes.text} >React Native</Typography>
                        <CircularProgressWithLabel value={native} />
                      </div>
                      <div className={classes.skills}>
                        <Typography color='secondary' className={classes.text} >TypeScript</Typography>
                        <CircularProgressWithLabel value={typescript} />
                      </div>
                      <div className={classes.skills}>
                        <Typography color='secondary' className={classes.text} >.Net core</Typography>
                        <CircularProgressWithLabel value={php} />
                      </div>
                      <div className={classes.skills}>
                        <Typography color='secondary' className={classes.text} >Python</Typography>
                        <CircularProgressWithLabel value={python} />
                      </div>
                    </Grid>
                  </Grid>
                </Grid>

              {/* IDIOMAS */}
                <Grid item xs={12} className={classes.titles} style={{marginTop: "20px"}}>
                  <div><BookIcon style={{ color: '#633974', marginTop: 10 }} /></div>
                  <Typography color='primary' className={classes.title}>Idiomas</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Divider style={{ backgroundColor: '#633974', marginBottom: 10 }} />
                </Grid>
                <Grid item xs={12} sm={12} md={12} >
                  <div style={{marginLeft: "30px"}}>
                    <Typography color='secondary' className={classes.text}>Español: Nativo</Typography>
                    <Typography color='secondary' className={classes.text}>Inglés: Intermedio</Typography>
                  </div>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid xs={12} md={2} />
    </Grid>
  )
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  };
}

function CircularProgressWithLabel(props) {
  const classes = styleCV();
  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress
        variant="determinate"
        className={classes.bottom}
        size={40}
        {...props}
        value={100}
      />
      <CircularProgress
        variant="determinate"
        className={classes.top}
        size={40}
        {...props}
      />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Typography variant="caption" component="div" color="textSecondary">{`${Math.round(
          props.value,
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

